<?php

namespace App\Model\NewsApi;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;

class ArticleResponse
{
    /**
     * @var string
     * @Type("string")
     */
    protected $status;

    /**
     * @var int
     * @Type("int")
     */
    protected $totalResults;

    /**
     * @var ArrayCollection
     * @Type("ArrayCollection<App\Model\NewsApi\Article>")
     */
    protected $articles;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getTotalResults(): int
    {
        return $this->totalResults;
    }

    /**
     * @param int $totalResults
     */
    public function setTotalResults(int $totalResults): void
    {
        $this->totalResults = $totalResults;
    }

    /**
     * @return ArrayCollection | Article[]
     */
    public function getArticles() : ArrayCollection
    {
        return $this->articles;
    }

    /**
     * @param ArrayCollection $articles
     */
    public function setArticles($articles): void
    {
        $this->articles = $articles;
    }
}