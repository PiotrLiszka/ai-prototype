<?php

namespace App\Model\NewsApi;
use JMS\Serializer\Annotation\Type;

class Source
{
    /**
     * @var integer|null
     * @Type("integer")
     */
    protected $id;

    /**
     * @var integer|null
     * @Type("string")
     */
    protected $name;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getName(): ?int
    {
        return $this->name;
    }

    /**
     * @param int|null $name
     */
    public function setName(?int $name): void
    {
        $this->name = $name;
    }
}