<?php

namespace App\Model\NewsApi;

use JMS\Serializer\Annotation\Type;

/**
 * Class Article
 *
 * {
 *   "source": {
 *     "id": null,
 *     "name": "Youbrandinc.com"
 *   },
 *   "author": "Scott Scanlon",
 *   "title": "Bitcoin Bitcoin Bitcoin BITCOIN!! So Sue Us.",
 *   "description": "Crypto Briefing exists to advocate for the safe and responsible integration of blockchain and cryptocurrency into mainstream life. We believe. Our goal is to grow the crypto community – to help new converts understand the basics, and to help more experienced …",
 *   "url": "https://www.youbrandinc.com/crytocurrency/bitcoin-bitcoin-bitcoin-bitcoin-so-sue-us/",
 *   "urlToImage": "https://www.youbrandinc.com/wp-content/uploads/2018/05/Bitcoin-Trademarked-In-The-UK-In-Patent-Troll-Style-Action-1024x538.jpg",
 *   "publishedAt": "2018-05-31T01:03:10Z"
 * }
 */
class Article
{
    /**
     * @var Source
     * @Type(Source::class)
     */
    protected $source;

    /**
     * @var string
     * @Type("string")
     */
    protected $author;

    /**
     * @var string
     * @Type("string")
     */
    protected $title;

    /**
     * @var string|null
     * @Type("string")
     */
    protected $description;

    /**
     * @var string
     * @Type("string")
     */
    protected $url;

    /**
     * @var string
     * @Type("string")
     */
    protected $urlToImage;
    /**
     * @var \DateTime
     * @Type("DateTime")
     */
    protected $publishedAt;

    /**
     * @return Source
     */
    public function getSource(): Source
    {
        return $this->source;
    }

    /**
     * @param Source $source
     */
    public function setSource(Source $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription():? string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrlToImage(): string
    {
        return $this->urlToImage;
    }

    /**
     * @param string $urlToImage
     */
    public function setUrlToImage(string $urlToImage): void
    {
        $this->urlToImage = $urlToImage;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt(): \DateTime
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     */
    public function setPublishedAt(\DateTime $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }
}