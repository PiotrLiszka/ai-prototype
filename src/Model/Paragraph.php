<?php

namespace App\Model;

class Paragraph
{
    const MIN_WORD_PER_SENTENCE = 5;

    /**
     * @var string | null
     */
    private $paragraph;

    /**
     * @var array | null
     */
    private $validSentences;

    /**
     * @var array
     */
    private $blackList = [
        'Price: CA$4',
        '✓',
        'Flickr',
        '�',
        '-----------------'
    ];

    public function __construct(?string $paragraph)
    {
        $this->paragraph = $paragraph;
    }

    public function getValidSentences()
    {
        if (is_array($this->validSentences)) {
            return $this->validSentences;
        }

        $sentences = $this->extractSentences($this->paragraph);
        $this->validSentences = [];
        foreach ($sentences as $item) {
            $string = $this->prettify($item);

            if($this->isSentenceValid($string)) {
                $this->validSentences[] = $string;
            }
        }

        return $this->validSentences;
    }

    private function extractSentences($string)
    {
        return explode('.', $string);
    }

    /**
     * @param null|string $validSentences
     * @return Paragraph
     */
    public function setValidSentences(?string $validSentences): Paragraph
    {
        $this->validSentences = $validSentences;
        return $this;
    }

    private function isSentenceValid($string)
    {
        if(empty($string))  {
            return false;
        }

        if (strlen($string) < 10) {
            return false;
        }

        $words = explode(' ', $string);

        if (count($words) < self::MIN_WORD_PER_SENTENCE) {
            return false;
        }

        if (preg_match('/[А-Яа-яЁё]/u', $string)) {
            return false;
        }

        foreach($this->blackList as $messyString) {
             if (strpos($string, $messyString) !== false) {
                 return false;
             }
        }
        return true;
    }

    private function prettify($string)
    {
        //remove new lines
        $string = str_replace("\n", '', $string);

        //more than one space
        $string = preg_replace('!\s+!', ' ', $string);

        //remove html tags:
        $string = html_entity_decode($string);

        //remove elements in brackets
        $string = preg_replace("/[\(|[|{][^[)|}|\]]+[\)|\]|}]/", "", $string);

        //remove elements between & and ;
        $string = preg_replace("/[&][\w^;]+[;]/", "", $string);

        //remove white chars at the end and ath the begining of the string
        $string = trim($string);

        return $string;
    }
}