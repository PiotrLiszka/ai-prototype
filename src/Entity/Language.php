<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguageRepository")
 */
class Language
{
    public function __construct()
    {
        $this->keywords = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $iso639_1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameEnglish;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $namePolish;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity=Keyword::class, cascade={"persist"})
     */
    private $keywords;

    /**
     * @ORM\OneToMany(targetEntity=Sentence::class,mappedBy="language")
     */
    private $sentences;

    public function getId()
    {
        return $this->id;
    }

    public function getIso6391(): ?string
    {
        return $this->iso639_1;
    }

    public function setIso6391(?string $iso639_1): self
    {
        $this->iso639_1 = $iso639_1;

        return $this;
    }

    public function getNameEnglish(): ?string
    {
        return $this->nameEnglish;
    }

    public function setNameEnglish(string $nameEnglish): self
    {
        $this->nameEnglish = $nameEnglish;

        return $this;
    }

    /**
     * @return Keyword[] | Collection
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setKeywords($keywords): self
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function attachKeyword(Keyword $keywords): self
    {
        $this->keywords->add($keywords);
        return $this;
    }

    /**
     * @return Sentence[]|ArrayCollection
     */
    public function getSentences()
    {
        return $this->sentences;
    }

    /**
     * @param ArrayCollection|Sentence[] $sentences
     */
    public function setSentences($sentences): void
    {
        $this->sentences = $sentences;
    }

    /**
     * @return mixed
     */
    public function getNamePolish()
    {
        return $this->namePolish;
    }

    /**
     * @param mixed $namePolish
     */
    public function setNamePolish($namePolish): void
    {
        $this->namePolish = $namePolish;
    }
}
