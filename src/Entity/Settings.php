<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SettingsRepository")
 */
class Settings
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Range(
     *      min = 10,
     *      max = 1100,
     *      minMessage = "Wartość musi być co najmniej równa {{ limit }}",
     *      maxMessage = "Wartość może być maksymalnie równa {{ limit }}"
     * )
     * @ORM\Column(type="integer")
     */
    private $maxDictionarySize;

    /**
     * @ORM\ManyToOne(targetEntity="EstimatorType", inversedBy="settings", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $estimatorType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $crossingValidation;

    /**
     * @ORM\Column(type="float")
     * @Assert\Range(
     *      min = 0,
     *      max = 0.8,
     *     )
     */
    private $randomSplitRatio;

    /**
     * @ORM\Column(type="boolean")
     */
    private $saveAndReuseSavedModel;

    public function getId()
    {
        return $this->id;
    }

    public function getMaxDictionarySize(): ?int
    {
        return $this->maxDictionarySize;
    }

    public function setMaxDictionarySize(int $maxDictionarySize): self
    {
        $this->maxDictionarySize = $maxDictionarySize;

        return $this;
    }

    public function getCrossingValidation(): ?bool
    {
        return $this->crossingValidation;
    }

    public function setCrossingValidation(bool $crossingValidation): self
    {
        $this->crossingValidation = $crossingValidation;

        return $this;
    }

    public function getEstimatorType(): EstimatorType
    {
        return $this->estimatorType;
    }

    public function setEstimatorType($estimatorType): void
    {
        $this->estimatorType = $estimatorType;
    }

    /**
     * @return mixed
     */
    public function getRandomSplitRatio()
    {
        return $this->randomSplitRatio;
    }

    /**
     * @param mixed $randomSplitRatio
     * @return Settings
     */
    public function setRandomSplitRatio($randomSplitRatio): Settings
    {
        $this->randomSplitRatio = $randomSplitRatio;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSaveAndReuseSavedModel()
    {
        return $this->saveAndReuseSavedModel;
    }

    /**
     * @param mixed $saveAndReuseSavedModel
     * @return Settings
     */
    public function setSaveAndReuseSavedModel($saveAndReuseSavedModel): Settings
    {
        $this->saveAndReuseSavedModel = $saveAndReuseSavedModel;
        return $this;
    }
}
