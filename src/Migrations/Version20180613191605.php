<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180613191605 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE keyword (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE settings (id INT AUTO_INCREMENT NOT NULL, estimator_type_id INT NOT NULL, max_dictionary_size INT NOT NULL, crossing_validation TINYINT(1) NOT NULL, INDEX IDX_E545A0C588ED3F6D (estimator_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sentence (id INT AUTO_INCREMENT NOT NULL, language_id INT DEFAULT NULL, value LONGTEXT NOT NULL, score VARCHAR(255) NOT NULL, INDEX IDX_9D664ED582F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, iso639_1 VARCHAR(2) DEFAULT NULL, name_english VARCHAR(255) NOT NULL, name_polish VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language_keyword (language_id INT NOT NULL, keyword_id INT NOT NULL, INDEX IDX_FE5EA66F82F1BAF4 (language_id), INDEX IDX_FE5EA66F115D4552 (keyword_id), PRIMARY KEY(language_id, keyword_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estimator_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(20) NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE settings ADD CONSTRAINT FK_E545A0C588ED3F6D FOREIGN KEY (estimator_type_id) REFERENCES estimator_type (id)');
        $this->addSql('ALTER TABLE sentence ADD CONSTRAINT FK_9D664ED582F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE language_keyword ADD CONSTRAINT FK_FE5EA66F82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE language_keyword ADD CONSTRAINT FK_FE5EA66F115D4552 FOREIGN KEY (keyword_id) REFERENCES keyword (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE language_keyword DROP FOREIGN KEY FK_FE5EA66F115D4552');
        $this->addSql('ALTER TABLE sentence DROP FOREIGN KEY FK_9D664ED582F1BAF4');
        $this->addSql('ALTER TABLE language_keyword DROP FOREIGN KEY FK_FE5EA66F82F1BAF4');
        $this->addSql('ALTER TABLE settings DROP FOREIGN KEY FK_E545A0C588ED3F6D');
        $this->addSql('DROP TABLE keyword');
        $this->addSql('DROP TABLE settings');
        $this->addSql('DROP TABLE sentence');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE language_keyword');
        $this->addSql('DROP TABLE estimator_type');
    }
}
