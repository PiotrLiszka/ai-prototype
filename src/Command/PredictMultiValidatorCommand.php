<?php

namespace App\Command;

use App\Entity\Settings;
use App\Repository\SettingsRepository;
use App\Service\MachineLearning\LanguagePredictValidator;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Container;

class PredictMultiValidatorCommand extends Command
{
    protected static $defaultName = 'predict:multi-validator';
    /**
     * @var LanguagePredictValidator
     */
    private $languagePredictValidator;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SettingsRepository
     */
    private $settingsRepository;
    /**
     * @var Container
     */
    private $container;

    public function __construct(
        ?string $name = null,
        SettingsRepository $settingsRepository,
        EntityManagerInterface $entityManager,
        LanguagePredictValidator $languagePredictValidator,
        ContainerInterface $container
    ) {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->settingsRepository = $settingsRepository;
        $this->languagePredictValidator = $languagePredictValidator;
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $range = [
            15,
            1000
        ];

        $step = 15;

        $settings = $this->settingsRepository->getFirstSetting();
        $backup = $settings->getMaxDictionarySize();
        $start = microtime(true);

        $filename = "{$settings->getEstimatorType()}-{$start}-validator.csv";
        $fp = fopen($filename, 'w');


        for ($size = $range[0]; $size <= $range[1]; $size += $step) {
            $startIterator = microtime(true);
            $settings->setMaxDictionarySize($size);
            $this->saveNewSetting($settings);

            $validatorInstance = $this->container->get('App\Service\MachineLearning\LanguagePredictValidator');
            $accuracy = $validatorInstance->validate();
            $endIterator = microtime(true);

            $x = sprintf('%02.4f, %s, %02.4f, %s', $endIterator - $startIterator, $size, $accuracy, $startIterator);
            $io->writeln($x);
            fwrite($fp, $x . "\n");

        }
        fclose($fp);

        $settings->setMaxDictionarySize($backup);
        $this->saveNewSetting($settings);
    }

    private function saveNewSetting(Settings $settings)
    {
        $this->entityManager->persist($settings);
        $this->entityManager->flush();
    }
}
