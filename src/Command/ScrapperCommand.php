<?php

namespace App\Command;

use App\Service\NewsApiScrapper;
use App\Service\ValidSentenceSaver;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ScrapperCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'scrapper:all-languages';
    /**
     * @var ValidSentenceSaver
     */
    private $validSentenceSaver;


    public function __construct(?string $name = null, ValidSentenceSaver $validSentenceSaver)
    {
        parent::__construct($name);
        $this->validSentenceSaver = $validSentenceSaver;
    }

    protected function configure()
    {
        $this
            ->setDescription('Get sentences for english languages');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->validSentenceSaver->scrapAllLanguages();

        $io->success('Done');
    }
}
