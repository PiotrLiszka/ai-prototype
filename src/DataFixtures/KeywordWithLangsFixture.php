<?php

namespace App\DataFixtures;

use App\Entity\Keyword;
use App\Entity\Language;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class KeywordWithLangsFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $languages = [
            [
                'plName' => 'Polski',
                'iso' => 'pl',
                'enName' => 'Polish',
                'keywords' => [
                    'muzyka',
                    'przyroda',
                    'rodzina',
                    'technologia',
                    'sport',
                    'samochody',
                    'państwo',
                ]
            ],

            [
                'plName' => 'Słowacki',
                'iso' => 'sk',
                'enName' => 'Slovakia',
                'keywords' => [
                    "Music",
                    "Narava",
                    'Družina',
                    "Tehnologija",
                    "Šport",
                    "Avtomobili",
                    "Država",
                ]
            ],
            [
                'plName' => 'Francuski',
                'iso' => 'fr',
                'enName' => 'French',
                'keywords' => [
                    'sport',
                    'repos',
                    'apprentissage',
                    'études',
                    'plantes',
                    'maison',
                    'internet',
                ]
            ],
            [
                'plName' => 'Niemiecki',
                'iso' => 'de',
                'enName' => 'Germany',
                'keywords' => [
                    'Sport',
                    'Lernen',
                    'Pflanzen',
                    'Haus',
                    'Familie',
                    'Autos',
                    'Technologie',
                ]
            ],
            [
                'plName' => 'Angielski',
                'iso' => 'en',
                'enName' => 'English',
                'keywords' => [
                    'game',
                    'sport',
                    'food',
                    'phones',
                    'computers',
                    'programming',
                    'work',
                ]
            ],
            [
                'plName' => 'Portugalski',
                'iso' => 'pt',
                'enName' => 'Portugal',
                'keywords' => [
                    'esporte',
                    'repos',
                    'apprentissage',
                    'études',
                    'plantes',
                    'maison',
                    'famille',
                ]
            ],
        ];


        foreach ($languages as $language) {

            $languageEntity = new Language();
            $languageEntity->setIso6391($language['iso']);
            $languageEntity->setNameEnglish($language['enName']);
            $languageEntity->setNamePolish($language['plName']);

            foreach ($language['keywords'] as $keyword) {
                $keywordEntity = new Keyword();
                $keywordEntity->setValue($keyword);
                $languageEntity->attachKeyword($keywordEntity);
            }

            $manager->persist($languageEntity);
        }
        $manager->flush();
    }
}
