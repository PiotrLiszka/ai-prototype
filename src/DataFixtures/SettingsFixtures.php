<?php

namespace App\DataFixtures;

use App\Entity\Settings;
use App\Entity\EstimatorType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ObjectManager;

class SettingsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $estimators = [
            'svc' => 'Support Vector Classification',
            'knearest' => 'KNearestNeighbors Classifier',
            'bayes' => 'NaiveBayes Classifier',
            'squares' => 'LeastSquares Linear Regression',
            'svr' => 'Support Vector Regression',
            'mlpc' => 'Multilayer Perceptron Classifier',
        ];

        foreach ($estimators as $type => $label) {
            $estimator = new EstimatorType();
            $estimator->setType($type);
            $estimator->setLabel($label);
            $manager->persist($estimator);
        }
        $manager->flush();

        $settings = new Settings();
        $settings->setCrossingValidation(false);
        $settings->setMaxDictionarySize(100);
        $settings->setSaveAndReuseSavedModel(false);
        $settings->setRandomSplitRatio(0.1);

        $estimatorRepo = $manager->getRepository(EstimatorType::class);
        $estimator = $estimatorRepo->getFirstElement();

        $settings->setEstimatorType($estimator);

        $manager->persist($estimator);
        $manager->persist($settings);
        $manager->flush();
    }
}
