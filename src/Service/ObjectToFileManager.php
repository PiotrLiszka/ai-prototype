<?php

namespace App\Service;

use Phpml\Exception\FileException;
use Phpml\Exception\SerializeException;

class ObjectToFileManager
{
    /**
     * @param $object
     * @param string $filepath
     * @throws FileException
     * @throws SerializeException
     */
    public function saveToFile($object, string $filepath): void
    {
        if (!is_writable(dirname($filepath))) {
            throw FileException::cantSaveFile(basename($filepath));
        }

        $serialized = serialize($object);
        if (empty($object)) {
            throw SerializeException::cantSerialize(gettype($object));
        }

        $result = file_put_contents($filepath, $serialized, LOCK_EX);
        if ($result === false) {
            throw FileException::cantSaveFile(basename($filepath));
        }
    }

    /**
     * @param string $filepath
     * @return object
     * @throws FileException
     * @throws SerializeException
     */
    public function restoreFromFile(string $filepath)
    {
        if (!file_exists($filepath) || !is_readable($filepath)) {
            throw FileException::cantOpenFile(basename($filepath));
        }

        $object = unserialize(file_get_contents($filepath));
        if ($object === false) {
            throw SerializeException::cantUnserialize(basename($filepath));
        }

        return $object;
    }
}
