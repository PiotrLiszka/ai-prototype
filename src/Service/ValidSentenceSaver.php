<?php

namespace App\Service;


use App\Entity\Language;
use App\Entity\Sentence;
use App\Repository\LanguageRepository;
use App\Repository\SentenceRepository;
use Doctrine\ORM\EntityManagerInterface;

class ValidSentenceSaver
{
    /**
     * @var NewsApiScrapper
     */
    private $scrapper;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SentenceRepository
     */
    private $sentenceRepository;
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    public function __construct(
        NewsApiScrapper $scrapper,
        EntityManagerInterface $entityManager,
        SentenceRepository $sentenceRepository,
        LanguageRepository $languageRepository
    ) {
        $this->scrapper = $scrapper;
        $this->entityManager = $entityManager;
        $this->sentenceRepository = $sentenceRepository;
        $this->languageRepository = $languageRepository;
    }

    public function getAndSave()
    {
        foreach ($this->scrapper->getSentences() as $sentence) {
            if ($this->sentenceRepository->findOneByValue($sentence) instanceof Sentence) {
                continue;
            }

            $sentenceEntity = new Sentence();
            $sentenceEntity->setValue($sentence);
            $sentenceEntity->setLanguage($this->scrapper->getLanguage());

            $this->entityManager->persist($sentenceEntity);
        }

        $this->entityManager->flush();
    }

    public function scrapAllLanguages()
    {
        $this->findAllLanguages(function (Language $language) {

            foreach ($this->scrapper->getSentences($language) as $sentence) {
                if ($this->sentenceRepository->findOneByValue($sentence) instanceof Sentence) {
                    continue;
                }

                $sentenceEntity = new Sentence();
                $sentenceEntity->setValue($sentence);
                $sentenceEntity->setLanguage($language);

                $this->entityManager->persist($sentenceEntity);
            }

            $this->entityManager->flush();
        });
    }

    private function findAllLanguages($callback)
    {
        foreach ($this->languageRepository->findAll() as $language) {
            $callback($language);
        }
    }
}