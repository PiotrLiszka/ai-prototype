<?php

namespace App\Service\MachineLearning;

use App\Entity\Settings;
use App\Repository\LanguageRepository;
use App\Repository\SettingsRepository;
use Doctrine\Common\Collections\Criteria;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Classification\NaiveBayes;
use Phpml\CrossValidation\RandomSplit;
use Phpml\Dataset\ArrayDataset;
use Phpml\Dataset\Dataset;
use Phpml\Estimator;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Math\Set;
use Phpml\Metric\ConfusionMatrix;
use Phpml\Tokenization\WordTokenizer;

use Phpml\Metric\Accuracy;
use Phpml\Classification\SVC;
use Phpml\SupportVectorMachine\Kernel;

class LanguagePredictValidator extends MachineLearningAbstract
{
    /**
     * @return float
     * @throws \Phpml\Exception\InvalidArgumentException
     */
    public function validate(): float
    {
        $dataset = $this->generateDictionaryUsingSettings();
        $newDataSet = $this->languageToTfIdfMatrix->extractDataset($dataset);

        $setting = $this->settingsRepository->getFirstSetting();
        $estimator = $this->createEstimator($dataset->getTargets());

        if ($setting->getCrossingValidation()) {
            $crossingValidation = new CrossingValidation($newDataSet, $estimator);
            $crossingValidation->validateModel();
            return $crossingValidation->getAverageAccuracy();
        } else {
            $randomSplit = new RandomSplit($newDataSet, $setting->getRandomSplitRatio());

            $estimator->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());
            $predictedLabels = $estimator->predict($randomSplit->getTestSamples());

            return Accuracy::score($randomSplit->getTestLabels(), $predictedLabels);
        }
    }

    public function getUsedLanguages()
    {
        return $this->usedLanguage;
    }

    public function getSourceSentences()
    {
        return $this->usedSentences;
    }

    public function confusionMatrix()
    {
        $dataset = $this->generateDictionaryUsingSettings();
        $newDataSet = $this->languageToTfIdfMatrix->extractDataset($dataset);

        $setting = $this->settingsRepository->getFirstSetting();
        $estimator = $this->createEstimator($dataset->getTargets());


        $randomSplit = new RandomSplit($newDataSet, $setting->getRandomSplitRatio());

        $estimator->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());
        $predictedLabels = $estimator->predict($randomSplit->getTestSamples());


//
        $filter = [];
        foreach ($this->getUsedLanguages() as $language) {
            $filter[] = $language->getNamePolish();
        }

        return ConfusionMatrix::compute($randomSplit->getTestLabels(), $predictedLabels, $filter);
    }

//
//    /**
//     * @return float|int
//     * @throws \Phpml\Exception\InvalidArgumentException
//     */
    private function validateLanguageModel()
    {
//        $this->getEstimator();
//        if ($this->settingsRepository->getFirstSetting()->getCrossingValidation()) {
//            $crossingValidation = new CrossingValidation($this->getDataset(), $this->estimator);
//            $crossingValidation->validateModel();
//            return $crossingValidation->getAverageAccuracy();
//
//        } else {
//            $randomSplit = new RandomSplit($this->getDataset(), $this->settings->getRandomSplitRatio());
//            $this->estimator->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());
//            $predictedLabels = $this->estimator->predict($randomSplit->getTestSamples());
//
//            return Accuracy::score($randomSplit->getTestLabels(), $predictedLabels);
//        }
    }
}