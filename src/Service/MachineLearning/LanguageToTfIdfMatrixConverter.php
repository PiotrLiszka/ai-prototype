<?php

namespace App\Service\MachineLearning;

use App\Entity\Language;
use App\Entity\Sentence;
use App\Entity\Settings;
use App\Repository\LanguageRepository;
use App\Repository\SentenceRepository;
use App\Repository\SettingsRepository;
use Doctrine\Common\Collections\Criteria;
use Phpml\Dataset\ArrayDataset;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;

class LanguageToTfIdfMatrixConverter
{

    /**
     * @var TokenCountVectorizer
     */
    private $countVectorizer;
    /**
     * @var TfIdfTransformer
     */
    private $tfIdfTransformer;

    public function __construct(
        TokenCountVectorizer $countVectorizer,
        TfIdfTransformer $tfIdfTransformer
    ) {
        $this->countVectorizer = $countVectorizer;
        $this->tfIdfTransformer = $tfIdfTransformer;
    }

    public function extractDataset(ArrayDataset $dataset)
    {
        $samples = $dataset->getSamples();
        # Budowanie słownika, rozdzielenie zdan na słowa

        $this->countVectorizer->fit($samples);

        # zrzutoawnie na wartosic 1/0

        $this->countVectorizer->transform($samples);

        # normalized tf or tf-idf representation

        $this->tfIdfTransformer->fit($samples);
        $this->tfIdfTransformer->transform($samples);

        # Wrzucenie do array dataset

        return new ArrayDataset($samples, $dataset->getTargets());
    }

    /**
     * @return TokenCountVectorizer
     */
    public function getCountVectorizer(): TokenCountVectorizer
    {
        return $this->countVectorizer;
    }

    /**
     * @return TfIdfTransformer
     */
    public function getTfIdfTransformer(): TfIdfTransformer
    {
        return $this->tfIdfTransformer;
    }
}