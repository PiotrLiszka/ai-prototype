<?php

namespace App\Service\MachineLearning;

use App\Entity\Settings;
use App\Repository\LanguageRepository;
use App\Repository\SettingsRepository;
use Doctrine\Common\Collections\Criteria;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Classification\NaiveBayes;
use Phpml\CrossValidation\RandomSplit;
use Phpml\Dataset\ArrayDataset;
use Phpml\Dataset\Dataset;
use Phpml\Estimator;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;

use Phpml\Metric\Accuracy;
use Phpml\Classification\SVC;
use Phpml\SupportVectorMachine\Kernel;

class CrossingValidation
{
    /**
     * @var Dataset
     */
    private $dataset;
    /**
     * @var Estimator
     */
    private $estimator;
    /**
     * @var int
     */
    private $splitParts;

    /**
     * @var array
     */
    private $partsAccuracy = [];
    /**
     * @var
     */
    private $sampleChunks;
    /**
     * @var
     */
    private $targetChunks;
    /**
     * @var int
     */
    private $averageAccuracy = 0;

    public function __construct(Dataset $dataset, Estimator $estimator, $splitParts = 10)
    {
        $this->dataset = $dataset;
        $this->estimator = $estimator;
        $this->splitParts = $splitParts;
    }

    public function validateModel()
    {
        $this->sampleChunks = $this->splitToChunks($this->dataset->getSamples());
        $this->targetChunks = $this->splitToChunks($this->dataset->getTargets());

        foreach ($this->sampleChunks as $i => $chunk) {
            [$sampleTrain, $targetTrain] = $this->getTrainElements($i);

            $this->estimator->train($sampleTrain, $targetTrain);

            [$sampleTest, $targetTest] = $this->getTestElement($i);
            $predictedTarget = $this->estimator->predict($sampleTest);

            $this->partsAccuracy[] = Accuracy::score($predictedTarget, $targetTest);
        }

        $this->setAverageAccuracy(array_sum($this->partsAccuracy) / count($this->partsAccuracy));
    }

    private function splitToChunks(array $data): array
    {
        $datasetSize = count($data);
        return array_chunk($data, $datasetSize / $this->splitParts);
    }

    /**
     * Return everything except items from current part
     * @param int $chunkIndex
     * @return array
     */
    private function getTrainElements(int $chunkIndex)
    {
        return [
            $this->flattenItemsExcludingOne($this->sampleChunks, $chunkIndex),
            $this->flattenItemsExcludingOne($this->targetChunks, $chunkIndex)
        ];
    }

    /**
     * Return element to test
     *
     * @param $i
     * @return array
     */
    private function getTestElement($i)
    {
        return [
            $this->sampleChunks[$i],
            $this->targetChunks[$i],
        ];
    }

    private function flattenItemsExcludingOne(array $data, $itemToExclude): array
    {
        unset($data[$itemToExclude]);

        return array_reduce($data, function ($carry, $item) {
            return array_merge($carry, $item);
        }, []);
    }

    /**
     * @return float
     */
    public function getAverageAccuracy(): float
    {
        return $this->averageAccuracy;
    }

    /**
     * @param float $averageAccuracy
     */
    public function setAverageAccuracy(float $averageAccuracy): void
    {
        $this->averageAccuracy = $averageAccuracy;
    }
}