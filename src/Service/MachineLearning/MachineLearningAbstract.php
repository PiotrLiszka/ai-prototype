<?php

namespace App\Service\MachineLearning;

use App\Entity\Language;
use App\Entity\Sentence;
use App\Entity\Settings;
use App\Repository\LanguageRepository;
use App\Repository\SentenceRepository;
use App\Repository\SettingsRepository;
use App\Service\ObjectToFileManager;
use Doctrine\Common\Collections\Criteria;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Classification\MLPClassifier;
use Phpml\Classification\NaiveBayes;
use Phpml\CrossValidation\RandomSplit;
use Phpml\Dataset\ArrayDataset;
use Phpml\Dataset\Dataset;
use Phpml\Estimator;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Math\Distance\Minkowski;
use Phpml\Math\Set;
use Phpml\NeuralNetwork\ActivationFunction\PReLU;
use Phpml\NeuralNetwork\ActivationFunction\Sigmoid;
use Phpml\NeuralNetwork\Layer;
use Phpml\NeuralNetwork\Node\Neuron;
use Phpml\Regression\LeastSquares;
use Phpml\Regression\SVR;
use Phpml\Tokenization\WordTokenizer;

use Phpml\Metric\Accuracy;
use Phpml\Classification\SVC;
use Phpml\SupportVectorMachine\Kernel;

abstract class MachineLearningAbstract
{
    /**
     * @var LanguageToTfIdfMatrixConverter
     */
    protected $languageToTfIdfMatrix;

    /**
     * @var SettingsRepository
     */
    protected $settingsRepository;

    /**
     * @var LanguageRepository
     */
    protected $languageRepository;
    /**
     * @var SentenceRepository
     */
    protected $sentenceRepository;
    /**
     * @var Language[]
     */
    protected $usedLanguage;
    /**
     * @var Sentence[]
     */
    protected $usedSentences;

    public function __construct(
        LanguageToTfIdfMatrixConverter $languageToTfIdfMatrix,
        SettingsRepository $settingsRepository,
        LanguageRepository $languageRepository,
        SentenceRepository $sentenceRepository
    ) {
        $this->languageToTfIdfMatrix = $languageToTfIdfMatrix;
        $this->settingsRepository = $settingsRepository;
        $this->languageRepository = $languageRepository;
        $this->sentenceRepository = $sentenceRepository;
    }

    /**
     * @param array $targets
     * @return Estimator
     * @throws \Phpml\Exception\InvalidArgumentException
     */
    protected function createEstimator(array $targets): Estimator
    {
        switch ($this->settingsRepository->getFirstSetting()->getEstimatorType()->getType()) {
            case 'svc':
                return new SVC(Kernel::RBF, 10000);
                break;
            case 'knearest':
                $k = floor(sqrt(count($targets)));
                return new KNearestNeighbors($k, new Minkowski());
                break;
            case 'bayes':
                return new NaiveBayes();
                break;
            case 'squares':
                return new LeastSquares();
                break;
            case 'svr':
                return new SVR(Kernel::LINEAR, $degree = 3, $epsilon = 10.0);
                break;
            case 'mlpc':
                $layer1 = new Layer(2, Neuron::class, new PReLU);
                $layer2 = new Layer(2, Neuron::class, new PReLU);

                return new MLPClassifier(4, [$layer1, $layer2], $targets);
                break;
        }
    }

    /**
     * @return ArrayDataset
     * @throws \Phpml\Exception\InvalidArgumentException
     */
    protected function generateDictionaryUsingSettings(): ArrayDataset
    {
        $samples = [];
        $targets = [];

        $maxSize = $this->settingsRepository->getFirstSetting()->getMaxDictionarySize();
        foreach ($this->languageRepository->findAll() as $language) {
            $sentences = $this->sentenceRepository->findRandomSentences($maxSize, $language);

            $this->usedLanguage[] = $language;
            foreach ($sentences as $sentence) {
                $this->usedSentences[] = $sentence;

                $samples[] = $sentence->getValue();
                $targets[] = $language->getNamePolish();
            };
        }

        return new ArrayDataset($samples, $targets);
    }

}