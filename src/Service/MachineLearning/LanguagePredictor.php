<?php

namespace App\Service\MachineLearning;

use App\Service\ObjectToFileManager;
use Phpml\Dataset\ArrayDataset;
use Phpml\Estimator;

class LanguagePredictor extends MachineLearningAbstract
{
    const CACHE_ESTIMATOR = 'estimator';
    const CACHE_DATASET = 'dataset';

    public function predict(string $sentence): string
    {
        $newSamples = [$sentence];
        $estimator = $this->getEstimator();

        $this->languageToTfIdfMatrix->getCountVectorizer()->transform($newSamples);
        $this->languageToTfIdfMatrix->getTfIdfTransformer()->transform($newSamples);

        $predicted = $estimator->predict($newSamples);
        return current($predicted);
    }

    public function trainNewUsingSettings($saveToCache = false): Estimator
    {
        $dataset = $this->generateDictionaryUsingSettings();

        $newDataSet = $this->languageToTfIdfMatrix->extractDataset($dataset);
        $estimator = $this->createEstimator($dataset->getTargets());

        $estimator->train($newDataSet->getSamples(), $newDataSet->getTargets());

        if ($saveToCache === true) {
            $modelManager = new ObjectToFileManager();

            $modelManager->saveToFile($dataset, self::CACHE_DATASET);
            $modelManager->saveToFile($estimator, self::CACHE_ESTIMATOR);
        }

        return $estimator;
    }

    protected function getEstimator()
    {
        if ($this->isCacheEnabled()) {
            return $this->restoreFromCache();
        } else {
            return $this->trainNewUsingSettings();
        }
    }
    protected function restoreFromCache(): Estimator
    {
        $modelManager = new ObjectToFileManager();

        $dataset = $modelManager->restoreFromFile(self::CACHE_DATASET);
        $this->languageToTfIdfMatrix->extractDataset($dataset);

        return $modelManager->restoreFromFile(self::CACHE_ESTIMATOR);
    }

    protected function isCacheEnabled()
    {
        return $this->settingsRepository->getFirstSetting()->getSaveAndReuseSavedModel();
    }
}