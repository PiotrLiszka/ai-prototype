<?php

namespace App\Service;

use App\Entity\Language;
use App\Model\Paragraph;
use Doctrine\Common\Collections\ArrayCollection;

class NewsApiScrapper
{
    const PAGES = 1;

    /**
     * @var NewsApiClient
     */
    private $apiClient;

    /**
     * @var ArrayCollection
     */
    private $sentenceCollection;

    public function __construct(NewsApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param Language $language
     * @return ArrayCollection | string[]
     */
    public function getSentences(Language $language): ArrayCollection
    {
        $this->setCollection();

        foreach ($language->getKeywords() as $keyword) {

            for ($page = 1; $page <= self::PAGES; $page++) {

                $articleResponse = $this->apiClient->everything($language->getIso6391(), $keyword->getValue(), $page);
                if (empty($articleResponse)) {
                    break;
                }

                foreach ($articleResponse->getArticles() as $article) {
                    $paragraph = new Paragraph($article->getDescription());

                    $paragraph->getValidSentences();

                    $this->addToCollection($paragraph->getValidSentences());
                }
            }
        }

        return $this->getCollection();
    }

    private function addToCollection(array $sentences)
    {
        foreach ($sentences as $sentence) {
            if (!empty($sentence)) {
                $this->sentenceCollection->add($sentence);
            }
        }
    }

    private function setCollection()
    {
        $this->sentenceCollection = new ArrayCollection();
    }

    private function getCollection()
    {
        return $this->sentenceCollection;
    }
}