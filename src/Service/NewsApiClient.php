<?php

namespace App\Service;

use App\Model\NewsApi\ArticleResponse;
use GuzzleHttp\Client;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;

class NewsApiClient
{
    const DEFAULT_LANG = 'en';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(Client $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    public function everything(string $lang = self::DEFAULT_LANG, string $keyword, int $page): ? ArticleResponse
    {
        $this->client->getConfig('query');

        $response = $this->client->get('everything', [
            'query' => array_merge($this->client->getConfig('query'), [
                'q' => $keyword,
                'page' => $page,
                'language' => $lang,
                'pageSize' => 100
            ])
        ]);

        return $this->deserializeResponse($response);
    }

    public function topHeadlines(string $country = self::DEFAULT_LANG, $page): ? ArticleResponse
    {
        $this->client->getConfig('query');

        $response = $this->client->get('top-headlines', [
            'query' => array_merge($this->client->getConfig('query'), [
                'country' => $country,
                'page' => $page,
                'pageSize' => 100
            ])
        ]);
        return $this->deserializeResponse($response);
    }

    private function deserializeResponse(ResponseInterface $response): ? ArticleResponse
    {
        $content = $response->getBody()->getContents();

        $deserialized = $this->serializer->deserialize($content, ArticleResponse::class, 'json');

        /** @var $deserialized  ArticleResponse */
        return $deserialized;
    }
}