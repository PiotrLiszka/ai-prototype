<?php

namespace App\Controller;

use App\Service\ValidSentenceSaver;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ScrapperController extends Controller
{
    /**
     * @Route("/scrapper", name="scrapper")
     * @param ValidSentenceSaver $validSentenceSaver
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function index(ValidSentenceSaver $validSentenceSaver)
    {
        $validSentenceSaver->scrapAllLanguages();

        return $this->redirectToRoute('dashboard');
    }
}
