<?php

namespace App\Controller;

use App\Entity\Settings;
use App\Form\Data\Predict;
use App\Form\PredictType;
use App\Form\SettingsType;
use App\Repository\LanguageRepository;

use App\Repository\SettingsRepository;
use App\Service\MachineLearning\LanguagePredictor;
use App\Service\MachineLearning\LanguagePredictValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DashboardController extends Controller
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var LanguagePredictor
     */
    private $languagePredictor;
    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    public function __construct(
        LanguageRepository $languageRepository,
        SettingsRepository $settingsRepository,
        LanguagePredictor $languagePredictor
    ) {
        $this->languageRepository = $languageRepository;
        $this->languagePredictor = $languagePredictor;
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * @Route("/", name="dashboard")
     * @param Request $request
     * @param string $appEnv
     * @return Response
     */
    public function index(Request $request, string $appEnv): Response
    {
        $languages = $this->languageRepository->findAll();
        $settings = $this->settingsRepository->getFirstSetting();

        $predictModel = new Predict();
        $formPredict = $this->createForm(PredictType::class, $predictModel);
        $formPredict->handleRequest($request);

        $predicted = null;

        if ($formPredict->isSubmitted() && $formPredict->isValid()) {
            $predicted = $this->languagePredictor->predict($predictModel->getContent());
        }

        $xdebug =  extension_loaded('xdebug');
        $sfEnv = $appEnv;


        return $this->render('dashboard/index.html.twig', [
            'languages' => $languages,
            'formPredict' => $formPredict->createView(),
            'predicted' => $predicted,
            'settings' => $settings,
            'cache' => [file_exists(LanguagePredictor::CACHE_ESTIMATOR), file_exists(LanguagePredictor::CACHE_DATASET)],
            'xdebug' => $xdebug,
            'sfEnv' => $sfEnv,
        ]);
    }
}
