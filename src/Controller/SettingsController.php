<?php

namespace App\Controller;

use App\Entity\Language;
use App\Entity\Settings;
use App\Form\SettingsType;
use App\Repository\SettingsRepository;
use App\Service\MachineLearning\LanguagePredictor;
use App\Service\MachineLearning\LanguagePredictValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SettingsController extends Controller
{

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;
    /**
     * @var LanguagePredictValidator
     */
    private $languagePredictorValidator;
    /**
     * @var LanguagePredictor
     */
    private $languagePredictor;

    public function __construct(
        SettingsRepository $settingsRepository,
        LanguagePredictValidator $languagePredictorValidator,
        LanguagePredictor $languagePredictor
    ) {
        $this->settingsRepository = $settingsRepository;
        $this->languagePredictorValidator = $languagePredictorValidator;
        $this->languagePredictor = $languagePredictor;
    }

    /**
     * @Route("/settings", name="settings")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $settings = $this->settingsRepository->getFirstSetting();
        if (!$settings instanceof Settings) {
            $settings = new Settings();
        }

        $formSettings = $this->createForm(SettingsType::class, $settings);
        $formSettings->handleRequest($request);


        if ($formSettings->isSubmitted() && $formSettings->isValid()) {
            if (file_exists(LanguagePredictor::CACHE_DATASET)) {
                unlink(LanguagePredictor::CACHE_DATASET);
            }
            if (file_exists(LanguagePredictor::CACHE_ESTIMATOR)) {
                unlink(LanguagePredictor::CACHE_ESTIMATOR);
            }

            $em = $this->getDoctrine()->getManager();

            if ($settings->getSaveAndReuseSavedModel()) {
                $this->languagePredictor->trainNewUsingSettings(true);
            }

            $em->persist($settings);
            $em->flush();
        }

        return $this->render('settings/index.html.twig', [
            'controller_name' => 'SettingsController',
            'formSettings' => $formSettings->createView(),
        ]);
    }

    /**
     * @Route("/validate", name="validate")
     * @return Response
     * @throws \Phpml\Exception\InvalidArgumentException
     */
    public function validate(): Response
    {
        $accuracy = round($this->languagePredictorValidator->validate(), 3);
        $usedLanguages = $this->languagePredictorValidator->getUsedLanguages();


        $confusionMtrxValidator = $this->container->get(LanguagePredictValidator::class);
        $confusionMatrix = $confusionMtrxValidator->confusionMatrix();
        $sourceSentences = $this->languagePredictorValidator->getSourceSentences();

        $usedLanguagesArray = $this->extractArrayLanguages($usedLanguages);


        return $this->render('settings/accuracy.html.twig', [
            'accuracy' => $accuracy,
            'confusionMatrix' => $confusionMatrix,
            'usedLanguageArray' => $usedLanguagesArray,
            'sourceSentences' => $sourceSentences,
        ]);
    }

    /**
     * @param $usedLanguages Language[]
     * @return array
     */
    private function extractArrayLanguages($usedLanguages)
    {
        $array = [];
        foreach ($usedLanguages as $language) {
            $array[] = $language->getNamePolish();
        }

        return $array;
    }
}
