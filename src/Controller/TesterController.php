<?php

namespace App\Controller;

use App\Service\MachineLearning\LanguagePredictor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TesterController extends Controller
{
    /**
     * @var LanguagePredictor
     */
    private $languagePredictor;

    public function __construct(LanguagePredictor $languagePredictor)
    {
        $this->languagePredictor = $languagePredictor;
    }

    /**
     * @Route("/tester", name="tester")
     */
    public function index()
    {
        $languages = [
                'Angielski' => [
                    'Division by zero.',
                    'Every morning she would listen to the radio.',
                    'This was something we were doing together.'
                ],
            'Francuski' => [
                'Sur le pont d’Avignon L\'on y danse tous en rond.',
                'L’important, c’est de ne jamais abandonner. Peu importe si cela paraît difficile, il ne faut jamais lâcher.',
                'Le monument au chef d’escadron Emmanuel Muller.'
            ],
            'Niemiecki' => [
                'Natürlich gibt es das alles nicht umsonst.',
                'Positiv',
                'Wir verschicken unsere besten News einmal am Tag'
            ],
            'Polski' => [
                'Bezapelacyjnie jednym z najszybszych seryjnych samochodów wciąż jest Bugatti Veyron.',
                'W zeszłym roku wakacje spędzili w Niemczech, tym razem wybiorą Francję.',
                'Krótkie zdanie.'
            ],
            'Portugalski' => [
                'Falhas na plataforma informática causam caos nas matrículas escolares',
                'Ministério Público confirma abertura de inquérito às alegadas fraudes na reconstrução',
                'Contabilistas'
            ],
            'Słowacki' => [
                'Je to ten najlepší deň na rozjímanie nad vecami.',
                'Rýchla poznámka.',
                'Slovenské juniorky a kadeti sa na ME mládeže v stolnom tenise v Kluži prebojovali zo základných skupín priamo do osemfinále medzi 16 najlepších tímov.',
            ],
        ];


        $predicts = [];
        foreach ($languages as $language => $sentences) {
            foreach ($sentences as $sentence) {

                $predictor = $this->container->get('App\Service\MachineLearning\LanguagePredictor');
                $predicted = $predictor->predict($sentence);

                $predicts[] = [
                    'sentence' => $sentence,
                    'language' => $language,
                    'predicted' => $predicted,
                    'valid' => $language === $predicted
                ];
            }
        }

        return $this->render('tester/index.html.twig', [
            'controller_name' => 'TesterController',
            'predicts' => $predicts,
        ]);
    }
}
