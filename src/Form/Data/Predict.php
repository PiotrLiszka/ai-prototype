<?php

namespace App\Form\Data;
use Symfony\Component\Validator\Constraints as Assert;

class Predict {
    /**
     * @Assert\NotBlank()
     * @var string|null
     */
    public $content;

    /**
     * @return string|null
     */
    public function getContent():? string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     */
    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

}