<?php

namespace App\Repository;

use App\Entity\Settings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Settings|null find($id, $lockMode = null, $lockVersion = null)
 * @method Settings|null findOneBy(array $criteria, array $orderBy = null)
 * @method Settings[]    findAll()
 * @method Settings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettingsRepository extends ServiceEntityRepository
{

    /**
     * @var Settings
     */
    private $settings;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Settings::class);
    }


    public function getFirstSetting(): ?Settings
    {
        if ($this->settings instanceof Settings) {
            return $this->settings;
        }

        return $this->createQueryBuilder('settings')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
