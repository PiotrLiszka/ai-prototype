<?php

namespace App\Repository;

use App\Entity\EstimatorType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EstimatorType|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstimatorType|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstimatorType[]    findAll()
 * @method EstimatorType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstimatorTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EstimatorType::class);
    }


    public function getFirstElement(): ?EstimatorType
    {
        return $this->createQueryBuilder('estimatorType')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
